# Generated by Django 3.1.4 on 2021-01-28 14:12

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alarmes', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='evento',
            name='Ambiente',
            field=models.CharField(default='', editable=False, max_length=100),
        ),
        migrations.AddField(
            model_name='evento',
            name='Area',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='Assignee',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='AssignmentGroup',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='Category',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='Contact',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='Description',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='EquipeCriador',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='Impact',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='IncidentID',
            field=models.CharField(default='', editable=False, max_length=100),
        ),
        migrations.AddField(
            model_name='evento',
            name='OpenTime',
            field=models.DateTimeField(default=datetime.datetime(2021, 1, 28, 11, 11, 59, 536388), editable=False),
        ),
        migrations.AddField(
            model_name='evento',
            name='OpenedBy',
            field=models.CharField(default='', editable=False, max_length=100),
        ),
        migrations.AddField(
            model_name='evento',
            name='Service',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='Status',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='Subarea',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='TelefoneSolicitante',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='Title',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='UpdateBy',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='UpdateTime',
            field=models.CharField(default='', editable=False, max_length=1000),
        ),
        migrations.AddField(
            model_name='evento',
            name='Urgency',
            field=models.CharField(default='', editable=False, max_length=100),
        ),
    ]
