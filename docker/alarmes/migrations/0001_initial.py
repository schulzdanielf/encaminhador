# Generated by Django 3.1.4 on 2021-01-28 13:51

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Evento',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action', models.CharField(max_length=1000)),
                ('data_evento', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Incidente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('action', models.CharField(max_length=1000)),
                ('equipe', models.CharField(choices=[('0', 'GSERV-AU'), ('1', 'DAT-SP'), ('2', 'GPROM-31'), ('3', 'GPROM-32'), ('4', 'GPROM-33'), ('5', 'GPROM-35'), ('6', 'GPROM-72')], default='1', max_length=1)),
                ('hostname', models.CharField(max_length=100)),
            ],
        ),
    ]
